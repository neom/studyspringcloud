package com.example.configclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//只有配置了actuator，RefreshScope才会生效

@RestController
@RefreshScope
public class TestController {

    @Value("${form}")
    private String form;

    @RequestMapping("/form")
    public String form(){
        return this.form;
    }


}
