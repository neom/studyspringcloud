package com.example.configclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;


@SpringBootApplication
@EnableDiscoveryClient
public class ConfigclientApplication {

	public static void main(String[] args) {

		SpringApplication.run(ConfigclientApplication.class, args);
//		new SpringApplicationBuilder(ConfigclientApplication.class).web(true).run(args);
	}
}
