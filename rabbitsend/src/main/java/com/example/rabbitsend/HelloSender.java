package com.example.rabbitsend;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HelloSender {
    @Autowired
    private AmqpTemplate template;

    public void send() {
        template.convertAndSend("queue","2222222");
        template.convertAndSend("queue","111111");
        template.convertAndSend("queue","33333");
        template.convertAndSend("queue","444444");
        template.convertAndSend("queue","5555555");
        template.convertAndSend("queue","6666666");
    }

    public void send1() {

        template.convertAndSend("exchange","topic.messages","444444");
    }
}
