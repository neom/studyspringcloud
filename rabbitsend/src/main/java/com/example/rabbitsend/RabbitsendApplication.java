package com.example.rabbitsend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitsendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RabbitsendApplication.class, args);
	}
}
