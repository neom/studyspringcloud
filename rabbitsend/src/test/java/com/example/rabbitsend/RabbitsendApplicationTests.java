package com.example.rabbitsend;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitsendApplicationTests {
	@Autowired
	private HelloSender helloSender;

    @Autowired
    private AmqpTemplate template;
	@Test
	public void contextLoads() {
		helloSender.send();
	}

	@Test
	public void testtopic() {
//		helloSender.send1();
        template.convertAndSend("exchange","topic.message","22222");
	}

    @Test
    public void testfanout() {
//		helloSender.send1();
        template.convertAndSend("fanoutExchange","","11111");
    }

}
