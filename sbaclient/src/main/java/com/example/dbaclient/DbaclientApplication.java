package com.example.dbaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication

public class DbaclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbaclientApplication.class, args);
	}
}
