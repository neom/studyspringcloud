package com.example.eureka;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "Base")
public class HelloWorldController {



    @RequestMapping(value="/helloWorld")
    public String helloWorld(){
        return "HelloWorld!";
    }


}
