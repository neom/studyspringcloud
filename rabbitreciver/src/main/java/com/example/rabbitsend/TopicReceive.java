package com.example.rabbitsend;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class TopicReceive {
    @RabbitListener(queues="topic.message")    //监听器监听指定的Queue
    public void process1(String str) {
        System.out.println("TopicReceive==message:"+str);
    }
    @RabbitListener(queues="topic.messages")    //监听器监听指定的Queue
    public void process2(String str) {
        System.out.println("TopicReceive==messages:"+str);
    }

    @RabbitListener(queues="topic.message.test")    //监听器监听指定的Queue
    public void process3(String str) {
        System.out.println("topic.message.test=========:"+str);
    }
}
