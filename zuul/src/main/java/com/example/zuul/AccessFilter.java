package com.example.zuul;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;

@Configuration
public class AccessFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx=RequestContext.getCurrentContext();
        HttpServletRequest request=ctx.getRequest();
        System.out.println("request====="+request.getMethod());
        System.out.println("request====="+request.getRequestURL().toString());
        Object accessToken= request.getParameter("accessToken");
        if(accessToken==null){
            System.out.println("accessToken 为空");
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            return null;
        }
        System.out.println("===accessToken is ok===");
        ctx.addZuulRequestHeader("Osso-User-Guid","46A60EAC350BA01AE0530A020397A01A");
        return null;
    }
}
