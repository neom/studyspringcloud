package com.example.ribbon;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@RestController
@RequestMapping(value = "Base")
public class ConsumerController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    HelloService helloService;

    //普遍的客户端负载均衡
    @RequestMapping(value="/ribbon-consumer")
    public String helloWorld(){

        return restTemplate.getForEntity("http://HELLO-SERVICE/Base/helloWorld",String.class).getBody();
    }

    //加断路
    @RequestMapping(value="/ribbon-hystrix")
    public String helloWorld1(){

        return helloService.helloService();
    }

    //加断路
    @RequestMapping(value="/test")
    public String test(HttpServletRequest request){
        System.out.println("========cookie=======");
        System.out.println(request.getCookies());
        return "test--ok";
    }

    //加断路
    @RequestMapping(value="/notest")
    public String notest(){

        return "notest--notest";
    }


    @RequestMapping(value="/qt")
    public String qt(HttpServletRequest request,HttpServletResponse response){
        System.out.println(request.getHeader("Osso-User-Guid"));
        try {
            request.getRequestDispatcher("http://qingtui-kaoqin.cisdi.com.cn:8010/CisdiOf/qt/searchkaoqinBySession").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
       // return restTemplate.getForEntity("http://qingtui-kaoqin.cisdi.com.cn:8010/CisdiOf/qt/searchkaoqinBySession",String.class).getBody();
    return "";
    }

    //


}
