package com.example.testmany;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureE {



    static  class MyCallable implements Callable<String>{
        @Override
        public String call() throws Exception {
            System.out.println("call=======");
            Thread.sleep(5000);
            return "okcall";
        }
    }

    public static void main(String[] args) throws Exception{
        ExecutorService ex=Executors.newCachedThreadPool();
//        Future<String>
        Future<String> fr=ex.submit(new MyCallable());
        Thread.sleep(1000);
        System.out.println("fr========"+fr.get());
    }
    //public static void
}
