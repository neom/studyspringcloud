package com.example.testmany;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class FutureTaskE {
    public static void main(String[] args) throws Exception{
        FutureTask<String> ft=new FutureTask<String>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("t---start----");
                Thread.sleep(5000);
                return "okcall";
            }
        });

        new Thread(ft).start();
        System.out.println("do something======");
        Thread.sleep(1000);
        String r=ft.get();
        System.out.println("r========="+r);
    }
}
