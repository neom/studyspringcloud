package com.example.testmany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestmanyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestmanyApplication.class, args);
	}
}
