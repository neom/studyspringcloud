package com.example.testcreator;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "Base")
public class HelloWorldController {

    @Autowired
    private DiscoveryClient client;

    @RequestMapping(value="/eeee")
    public String eeee(){
        System.out.println("client.getServices"+client.getServices());
        return "eeeeeeeee!";
    }

    @RequestMapping(value="/helloWorld")
    public String helloWorld(){
        System.out.println("client.getServices"+client.getServices());
        return "HelloWorld!";
    }

    @GetMapping("/product/{id}")
    public String getProduct(@PathVariable String id) {
        //for debug
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "product id : " + id;
    }

    @GetMapping("/order/{id}")
    public String getOrder(@PathVariable String id) {
        //for debug
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "order id : " + id;
    }


}
