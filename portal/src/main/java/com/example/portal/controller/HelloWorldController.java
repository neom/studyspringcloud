package com.example.portal.controller;


import com.alibaba.fastjson.JSONObject;
import com.example.portal.mapper.TestMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@RestController
@RequestMapping(value = "Base")
public class HelloWorldController {

    @Autowired
    private TestMapper testMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @RequestMapping(value="/helloWorld")
    public String helloWorld(){
        return "HelloWorld!";
    }

    @RequestMapping(value="/testcode")
    public String testcode(HttpServletRequest request){
        String qtcode=request.getParameter("qt_code");
        return qtcode;
    }

    @RequestMapping(value="/select")
    @ResponseBody
    public String select(@RequestBody Map<String, Object> params){
//        System.out.println(testMapper.selecBySql("select sysdate from dual"));
        String sql=(String)params.get("SQL");
        System.out.println("select1031===sql："+sql);
        String s = JSONObject.toJSONString(testMapper.selecBySql(sql));
        return s;
    }

    @RequestMapping(value="/selectCache")
    @ResponseBody
    public String selectCache(@RequestBody Map<String, Object> params){
//        System.out.println(testMapper.selecBySql("select sysdate from dual"));
        String sql=(String)params.get("SQL");
        String ca=redisTemplate.opsForValue().get(sql);

        //先从缓存取缓存没有取oracle数据库
        if(!"".equals(ca)&&ca!=null){
            log.info("cach===");
            return ca;
        }

        log.info("select1101===sql："+sql);


        String s = JSONObject.toJSONString(testMapper.selecBySql(sql));
        //写入redis中
        redisTemplate.opsForValue().set(sql,s);
//        redisTemplate.opsForValue().set("1","2");
        return s;
    }

    @RequestMapping(value="/execute")
    @ResponseBody
    public String execute(@RequestBody Map<String, Object> params){
        String sql=(String)params.get("SQL");
        System.out.println("execute==当前获取到的sql："+sql);
        String s = JSONObject.toJSONString(testMapper.executeBySql(sql));
        return s;
    }

    @RequestMapping(value="/executels")
    @ResponseBody
    public String executels(@RequestBody Map<String, Object> params){
        List<String> SqlArray=(ArrayList<String>)params.get("SqlArray");
        int i=0;
        for (String sql : SqlArray) {

            try {
                if(testMapper.executeBySql(sql)){
                    i++;
                }
            } catch (Exception e) {

                continue;
            }
        }
        System.out.println("execute==当前获取到的sql："+SqlArray);
        String s = JSONObject.toJSONString(i);
        return s;
    }








}
