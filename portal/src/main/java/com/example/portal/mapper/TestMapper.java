package com.example.portal.mapper;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface TestMapper {
    @Select("${value}")
    public List<Map<String, Object>> selecBySql(String sql);

    @Insert("${value}")
    public boolean executeBySql(String sql);
}
